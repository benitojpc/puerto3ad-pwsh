<#
    .SYNOPSIS
        Gestión de usuarios en Active Directory (altas, bajas, consultas y modificaciones)

	.DESCRIPTION
        Para la creación de un usuario leera los datos de un fichero CSV, este contendrá
        los siguientes campos separados por ";":
            * nombre
            * apellido1
            * apellido2
            * descripcion
            * memberof

        El campo "memberof" es una cadena que contendrá un grupo
        o varios grupos, en este caso estarán separados por el carácter
        de ":".

    .PARAMETER
        No recibe parámetros, se define dentro de script la llamada al fichero csv

    .INPUTS
        En el caso de altas como de bajas, los usuarios se asignarán mediante un
        fichero csv con los datos de los usuarios a dar de alta o de baja.

    .OUTPUTS
        No hay ningun valor de salida

	.NOTES
        Version.......: 1.0
        Author........: Benito J. Palacios
        Creation Date.: 22 Febrero 2022
        Purpose/Change: Gestión Active Directory Puerto3
        Name script...: user_services.psm1

	.EXAMPLE
        es un módulo que se llama desde un programa principal

#>

using module "d:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\modelos\userad.psm1"

class UserService 
{

    hidden [UserAD[]] static $usuarios
    hidden [string] static $fout = "D:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\csvs\usuarios_existentes.csv"

    hidden [String]   static $AD_SRV       = "192.168.5.21"
    hidden [string]   static $AD_BASE_USER = "OU=PUERTO3,OU=Comunes,OU=USUARIOS"
    hidden [string]   static $AD_BASE      = "dc=iipp,dc=int"
    hidden [string]   static $AD_DOMAIN    = "iipp.int"
    hidden [string[]] static $AD_OBJ_CLASS = @('top', 'person', 'organizationalPerson', 'user')
    hidden [string[]] Static $AD_GRPS      = @("CN=map_conecta,OU=Grupos Comunes,OU=USUARIOS,DC=iipp,DC=int",
                                                "CN=Puerto3,OU=PUERTO3,OU=Comunes,OU=USUARIOS,DC=iipp,DC=int")

    [void] static AddUserToList( [object] $reg )
    {
        $uad = UserAD

        $aDatos = @($reg.nombre, $reg.apellido1, $reg.apellido2)
        $samacc = [UserService]::create_samacc($aDatos)

        $uad.SetValue( 'sAMAccountName', $samacc )
        $uad.SetValue( 'name', $samacc )
        $uad.SetValue( 'givenname', $reg.nombre )
        $uad.SetValue( 'sn', ("{0} {1}" -f $reg.apellido1, $reg.apellido2) )
        $uad.SetValue( 'description', $reg.descripcion )
        $uad.SetValue( 'userPrincipalName', ("{0}@iipp.int" -f $samacc) )
        $uad.SetValue( 'displayName', $samacc )
        $uad.SetValue( 'path', ("{0},{1}" -f [UserService]::AD_BASE_USER,[UserService]::AD_BASE) )
        $uad.SetValue( 'memberOf', [UserService]::get_grupos($reg.memberof) )
        
        [UserService]::usuarios += $uad
    }

    [void] static ShowUsers() 
    {
        $contador = 1
        foreach( $uad in [UserService]::usuarios )
        {
            Write-Host("Registro: {0}" -f $contador)
            $uad.Show()
            Write-Host("-----------------------------------------------")
            $contador += 1
        }
    }

    [Hashtable] static find_user( [string] $uname, [string[]] $keys)
    {
        $registro = @{}
        $secpasswd = "bjpc@1961" | ConvertTo-SecureString -AsPlainText -Force
        $cred = New-Object System.Management.Automation.PSCredential ("benito palacios", $secpasswd)
        $serv    = ("{0}" -f [UserService]::AD_SRV)
        $filtro = ("samaccountname -eq '{0}'" -f $uname)
        $user = Get-ADUser -Server $serv -Filter $filtro -Credential $cred
        if ($null -ne $user)
        {
            foreach ($key in $keys)
            {
                $registro += @{$key = $user.$key}
            }
        }
        return $registro
    }

    [void] static AddUser( [object] $reg )
    {
        $aDatos = @($reg.nombre, $reg.apellido1, $reg.apellido2)
        $aGrupos = [UserService]::get_grupos($reg.memberof)

        $samacc = [UserService]::create_samacc($aDatos)
        if ("" -ne $samacc) 
        {
            $passwd = "cppuerto@3" | ConvertTo-SecureString -AsPlainText -Force
            $secpasswd = "bjpc@1961" | ConvertTo-SecureString -AsPlainText -Force
            $cred = New-Object System.Management.Automation.PSCredential ("iipp\benito palacios", $secpasswd)
            $serv = ("{0}" -f [UserService]::AD_SRV)
            $descr= ("{0}" -f $reg.descripcion)

            New-ADUser -Server $serv -Credential $cred `
                        -Name ("{0}" -f [UserService]::capitalize($samacc)) `
                        -GivenName ("{0}" -f [UserService]::capitalize($reg.nombre)) `
                        -Surname ("{0} {1}" -f [UserService]::capitalize($reg.apellido1),[UserService]::capitalize($reg.apellido2)) `
                        -sAMAccountName ("{0}" -f $samacc) `
                        -Description $descr `
                        -UserPrincipalName ("{0}@{1}" -f $samacc, [UserService]::AD_DOMAIN) `
                        -Displayname ("{0}" -f [UserService]::capitalize($samacc)) `
                        -Enabled $true `
                        -AccountPassword $passwd `
                        -ChangePasswordAtLogon $true `
                        -Company "Centro Penitenciario Puerto III" `
                        -Title "S.G.II.PP" `
                        -State "Cadiz" `
                        -Country "es" `
                        -City "El Puerto de Santa Maria" `
                        -PostalCode "11500" `
                        -Office ("{0}" -f $reg.oficina) `
                        -Department ("{0}" -f $reg.departamento) `
                        -Path ("{0},{1}" -f [UserService]::AD_BASE_USER, [UserService]::AD_BASE)

            
            foreach( $grupo in $aGrupos )
            {
                $grupo | Add-ADGroupMember -Server $serv -Credential $cred -Members $samacc
            }
        }
    }

    hidden [string] static create_samacc( [string[]] $aSam)
    {
        [string[]] $keys = @('DistinguishedName')
        $paso1 = $TRUE
        $sigue = $TRUE
        $num   = 1
    
        if ( ($aSam[0].Split(" ")).Length -gt 1 ) 
        {
            $nombre = $aSam[0].Split(" ")
            $SamaccTemp = ("{0}{1} {2}" -f $nombre[0].Substring(0,1).ToLower(),$nombre[1].ToLower(), $aSam[1].ToLower())
        } else 
        {
            $SamaccTemp = ("{0} {1}" -f $aSam[0].ToLower(), $aSam[1].ToLower())
        }
    
        while ($sigue)
        {
            $datos = [UserService]::find_user($SamaccTemp, $keys) #Get-ADUser -SamAccountName $SamaccTemp
            if ($datos.Count -gt "0")
            {
                
                if ($datos['DistinguishedName'].ToLower() -like '*puerto3*')
                {
                    $file = [UserService]::fout
                    Add-Content -Path $file ("{0};{1}" -f $SamaccTemp, $datos['DistinguishedName'])
                    #$SamaccTemp = $null
                    #$sigue = $FALSE
                    if ($paso1)
                    {
                        $sApel = $aSam[2][0]
                        $SamaccTemp = -join($SamaccTemp,$sApel)
                        $paso1 = $FALSE
                    }
                    else 
                    {
                        $SamaccTemp = -join($SamaccTemp,$num.ToString())
                        $num += 1    
                    }
                } elseif ($paso1)
                {
                    # debemos añadir la inicial 2º apellido
                    $sApel = $aSam[2][0]
                    $SamaccTemp = -join($SamaccTemp,$sApel)
                    $paso1 = $FALSE
                }
                else 
                {
                    # debemos añadir números consecutivos
                    $SamaccTemp = -join($SamaccTemp,$num.ToString())
                    $num += 1
                }
            }
            else 
            {
                $sigue = $FALSE
            }
        }
        return $SamaccTemp
    }

    hidden [string] static get_dn( [string] $samacc)
    {
        return ("CN={0},{1},{2}" -f $samacc,[UserService]::ad_base_user, [UserService]::ad_base)
    }

    hidden [String] static capitalize([string] $texto)
    {
        ( $texto -split " " |ForEach-Object {
            if($_ -notin [UserService]::NoCapitalization){
                "$([char]::ToUpper($_[0]))$($_.Substring(1))"
            } else { $_ }
        }) -join " "
        return $texto
    }

    hidden [System.Collections.ArrayList] static get_grupos([string] $grps)
    {
        $aGrps    = $grps.Split(":") 
        [string[]] $lista = [UserService]::AD_GRPS

        foreach ($grp in $aGrps) 
        {
            $txt = ("CN={0},{1},{2}" -f $grp,[UserService]::AD_BASE_USER, [UserService]::AD_BASE)
            $lista += $txt.Trim()
        } 
        return $lista
    }

}
