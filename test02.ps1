<#
    .SYNOPSIS
        Programa de prueba para comprobar la gestión de A.D.

	.DESCRIPTION
        Desde este programa se llamará al módulo de gestión de usuarios en A.D.

    .PARAMETER
        No recibe parámetros

    .INPUTS
        Se proporciona la ruta al fichero csv con los datos a incluir

    .OUTPUTS
        No hay ningun valor de salida

	.NOTES
        Version.......: 1.0
        Author........: Benito J. Palacios
        Creation Date.: 22 Febrero 2022
        Purpose/Change: Gestión Active Directory Puerto3
        Name Script...: test02.ps1

	.EXAMPLE
        .\test02.ps1 "D:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\csvs\usuarios.csv" 
#>

using module "d:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\services\user_services.psm1"

Import-Module ActiveDirectory 

#$csvfile = $args[0]

function ReadCSVFile ( [string] $fcsv)
{
    #$fout = ("csvs/{0}" -f "users_utf8.csv")
    #Get-Content $fcsv | Out-File $fout -Encoding utf8
    #[object[]]$registros  = Import-Csv -Delimiter ";" -Path $fout
    [object[]]$registros  = Import-Csv -Delimiter ";" -Path $fcsv
    foreach( $reg in $registros)
    {
        #[UserService]::AddUserToList( $reg )
        [UserService]::AddUser( $reg )
    }
    #[UserService]::ShowUsers()
}


#ReadCSVFile $csvfile
ReadCSVFile "D:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\csvs\usuarios.csv"