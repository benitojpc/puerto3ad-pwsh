<#
    .SYNOPSIS
        Programa de prueba para comprobar la gestión de A.D.

	.DESCRIPTION
        Desde este programa se llamará al módulo de gestión de usuarios en A.D.

    .PARAMETER
        No recibe parámetros

    .INPUTS
        Se proporciona la ruta al fichero csv con los datos a incluir

    .OUTPUTS
        No hay ningun valor de salida

	.NOTES
        Version:        1.0
        Author:         Benito J. Palacios
        Creation Date:  22 Febrero 2022
        Purpose/Change: Gestión Active Directory Puerto3

	.EXAMPLE
        [UserService]::add_users("D:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\csvs\usuarios.csv")        
#>
using module "d:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\services\user_services.psm1"
#using module ./services/user_services.psm1

Import-Module ActiveDirectory

#[UserService]::BuscarUsuario("benito palacios")
[UserService]::add_users("D:\Programs\Pwsh\EnDesarrollo\puerto3ad\puerto3ad-pwsh\csvs\usuarios.csv")