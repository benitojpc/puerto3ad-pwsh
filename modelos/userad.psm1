<#
    .SYNOPSIS
        Módulo que define la clase UserAD para la gestión de los
        atributos de los usuarios en AD.

	.DESCRIPTION
        Contiene los atributos y valores del usuario que se ha leído 
        desde Active Directory

    .PARAMETER
        No recibe parámetros

    .INPUTS
        Valores leídos en A.D.

    .OUTPUTS
        Valores almacenados en la clase

	.NOTES
        Version:        1.0
        Author:         Benito J. Palacios
        Creation Date:  22 Febrero 2022
        Purpose/Change: Gestión Active Directory Puerto3
        Name script:    userad.psm1

	.EXAMPLE
        [UserAD]::GetAttribs() --> devuelve los atributos que se pueden recuperar
        [UserAD]::Show() ---> muestra los valores y atributos del objeto.
#>
class UserAD
{
    [Hashtable] $attrs  = @{}
    [Hashtable] $values = @{}

    UserAD()
    {
        $this.attrs = @{
                        "accountExpires"    = "Expira";
                        "cn"                = "Common Name";
                        "description"       = "Description";
                        "displayName"       = "Display Name";
                        "distinguishedName" = "DN";
                        "givenName"         = "Given Name";
                        "lastLogon"         = "Last Logon";
                        "lastLogonTimestamp"= "Last Logon Time";
                        "memberOf"          = "Member of";
                        "name"              = "Name";
                        "pwdLastSet"        = "Password Last Set";
                        "sAMAccountName"    = "sAMAccount Name";
                        "sn"                = "Surname";
                        "userPrincipalName" = "User Principal Name";
                        "path"              = "Path";
                        "whenChanged"       = "When changed";
                        "whenCreated"       = "When created";
                    }
        $this.InitValues()
    }

    hidden [void] InitValues() 
    {
        foreach ($key in $this.attrs.Keys) 
        {
            $this.values[$key] = ""
        }
    }

    [string[]] GetAttribs() 
    {
        return [string[]] $this.attrs.Keys
    }

    [string] GetValue([string] $key)
    {
        return $this.values[$key]
    }

    [void] SetValue( [string] $key, [string] $value)
    {
        $this.values[$key] = $value
    }

    [void] Show() 
    {
        foreach ($key in $this.attrs.Keys) 
        {
            if (![string]::IsNullOrEmpty($this.values[$key]))
            {
                if ($key -eq "memberOf") 
                {
                    $primero = $true
                    $grupos  = ($this.values[$key]).Split("|")
                    
                    foreach ($grupo in $grupos) 
                    {
                        if ($primero)
                        {
                            $primero = $false 
                            Write-Host("{0,20}: {1}" -f $key, $grupo)
                        } else 
                        {
                            Write-Host("{0,20} {1}" -f " ",$grupo)
                        }
                    }
                } else 
                {
                    Write-Host("{0,20}: {1}" -f $key, $this.values[$key])
                }
            }
        }
    }

}

function UserAD () 
{
    return [UserAD]::new()
}

Export-ModuleMember UserAD